import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { NavController } from '@ionic/angular';
import { KostService } from '../tab1/kost.service';
import { Kost } from '../tab1/kost.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.page.html',
  styleUrls: ['./transaction.page.scss'],
})
export class TransactionPage implements OnInit {
  kost: Kost;

  constructor(
    private _location: Location,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private kostService: KostService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if(!paramMap.has('kostId')) {
        this.navCtrl.navigateBack('/tabs/tab1');
        return;
      }

      this.kost = this.kostService.getKost(
        paramMap.get('kostId')
        )
    });
    console.log(this.kost);
  }

}

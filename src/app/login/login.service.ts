import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private userIsAuthenticated = true;
  private userId = 'abc';

  get isAuthenticated() {
    return this.userIsAuthenticated;
  }

  get id() {
    return this.userId;
  }

  constructor() {}

  login() {
    this.userIsAuthenticated = true;
  }

  logout() {
    this.userIsAuthenticated = true;
  }
}

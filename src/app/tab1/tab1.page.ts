import { Component } from '@angular/core';
import { KostService } from './kost.service';
import { Kost } from './kost.model';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  kostData: Kost[];
  kosts: Array<any>;

  constructor(private KostService: KostService) {}

  ngOnInit() {
    this.kosts = this.KostService.kosts;
    console.log(this.kosts);
  }

}

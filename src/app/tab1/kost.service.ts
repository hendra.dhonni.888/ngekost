import { Injectable } from '@angular/core';
import { Kost } from './kost.model';
@Injectable({
  providedIn: 'root'
})
export class KostService {
  private _kost = [
    new Kost(
      'murah-meriah',
      'Murah Meriah',
      [
        {
          id: 'murah-meriah_1',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost tiga belas',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'murah-meriah_2',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost Anggrek 3',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'murah-meriah_3',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost Nuansa',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'murah-meriah_4',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg',
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
          ],
          kostName: 'Kost mumur',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'murah-meriah_5',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost salsabila',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'murah-meriah_6',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost Kevin',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
      ]
    ),
    new Kost(
      'lagi-promo',
      'Lagi Promo',
      [
        {
          id: 'lagi-promo_1',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict1L/Y4/Y953594/Y953594007.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'patriot KOST',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'lagi-promo_2',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict1L/Y6/Y990786/Y990786001.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost Anggrek 3',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'lagi-promo_3',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict1L/Y8/Y959608/Y959608010.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'the murah kost',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'lagi-promo_4',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict1L/Y6/Y964156/Y964156014.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'kost melati',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'lagi-promo_5',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict2L/Y8/Y959608/Y959608008.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'kost wibowo',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'lagi-promo_6',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict2L/Y8/Y959608/Y959608008.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'kost UWU',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
      ]
    ),
    new Kost(
      'kost-ump',
      'Deket UMP',
      [
        {
          id: 'kost-ump_1',
          imgUrl: [
            'https://origin.pegipegi.com/jalan/images/pict1L/Y6/Y964156/Y964156014.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'kost dua sodara',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-ump_2',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost sepi',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-ump_3',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost bagus',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-ump_4',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost gundam',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-ump_5',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost captain nazi',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-ump_6',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'kost undai',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
      ]
    ),
    new Kost(
      'kost-unsoed',
      'Deket Unsoed',
      [
        {
          id: 'kost-unsoed_1',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost saya murah banget',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-unsoed_2',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'mamiKost',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-unsoed_3',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost empat saudari',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-unsoed_4',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost sparta',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-unsoed_5',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost Nobu',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
        {
          id: 'kost-unsoed_6',
          imgUrl: [
            'https://www.trbimg.com/img-5849a7ba/turbine/ct-elite-street-naperville-pricey-listing-1211-biz-20161208',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375006.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375004.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375005.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375003.jpg',
            'https://origin.pegipegi.com/jalan/images/pict2L/Y5/Y994375/Y994375007.jpg'
          ],
          kostName: 'Kost tokugawa',
          kostAddress: 'Jl. Raya',
          rating: 4,
          price: 2000000,
          paymentDuration: 'tahun',
          contactPerson: '081',
          totalRoom: 50,
          availableRoom: 20,
          facility: [
            'Kamar Mandi Dalam',
            'Wi-FI gratis',
            'Sarapan Gratis'
          ]
        },
      ]
    ),
  ];

  constructor() { }

  get kosts() {
    return [...this._kost];
  }

  getKostSection(id: string) {
    return {...this._kost.find(kost => kost.id === id)};
  }

  getKost(kostId: string) {
    const sectionId = kostId.split("_")[0];
    const section =  {...this._kost.find(kost => kost.id === sectionId)};
    return {...section.section.find(card => card.id === kostId)};
  }

  findKost(value: string) {
    let result = [];
    const lowerCasedValue = value.toLowerCase();
    this._kost.forEach(section => {
      section.section.forEach(card => {
        if(card.kostName.toLowerCase().includes(lowerCasedValue)) {
          result.push(card);
        }
      });
    });
    return result;
  }
}

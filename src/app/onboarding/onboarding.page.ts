import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.page.html',
  styleUrls: ['./onboarding.page.scss'],
})
export class OnboardingPage implements OnInit {
  @ViewChild(IonSlides) slides: IonSlides;

  constructor(private router: Router) { }

  ngOnInit() {
    const isOnboarded = localStorage.getItem('isOnboarded');
    if (JSON.parse(isOnboarded)) {
      this.router.navigate(['/', 'login']);
    } else {
      localStorage.setItem('isOnboarded', 'true');
    }
  }

  next() {
    this.slides.slideNext();
  }

  back() {
    this.slides.slidePrev();
  }

}

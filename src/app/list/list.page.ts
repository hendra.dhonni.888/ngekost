import { Component, OnInit } from '@angular/core';
import { KostService } from '../tab1/kost.service';
import { Kost } from '../tab1/kost.model';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  kosts: Kost;
  searchInput: string = '';

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private kostService: KostService
    ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if(!paramMap.has('id')) {
        this.navCtrl.navigateBack('/tabs/tab1');
        return;
      }

      this.kosts = this.kostService.getKostSection(paramMap.get('id'))
    });
  }

  findKost() {
    let data = this.kostService.findKost(this.searchInput)
    this.kosts = {...this.kosts, section: data};
  }

}

import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { NavController } from '@ionic/angular';
import { KostService } from '../tab1/kost.service';
import { Kost } from '../tab1/kost.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  kost: Kost;
  firstImage: any;

  constructor(
    private _location: Location,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private kostService: KostService
    ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if(!paramMap.has('kostId')) {
        this.navCtrl.navigateBack('/tabs/tab1');
        return;
      }

      this.kost = this.kostService.getKost(
        paramMap.get('kostId')
        )
    });
    console.log(this.kost['imgUrl'][0]);
    this.firstImage = this.kost['imgUrl'][0];
  }

  backClicked() {
    this._location.back();
  }

  changeImage(img){
    this.firstImage = img;
  }
}

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(
    private loginService: LoginService,
    private router: Router
  ) {}

  onLogout() {
    this.loginService.logout();
    this.router.navigateByUrl('/login');
  }

}
